package pex4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.sound.sampled.Clip;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

/**
 *
 * @author James Wienold
 */
public class OptionsFrame extends JFrame {

    private JButton beginnerSelection = new JButton("Beginner");
    private JButton intermediateSelection = new JButton("Intermediate");
    private JButton advancedSelection = new JButton("Advanced");
    private JButton helpButton = new JButton("Have no idea what Minesweeper is? Click here to find out!");
    private ImageIcon yuisis = new ImageIcon(getClass().getClassLoader().getResource("images/yuisis.png"));
    private JLabel welcomeLabel = new JLabel("Welcome to Anime Minesweeper!");
    private JLabel countLabel = new JLabel("Pick how long you would like the posion timer to be: ");
    private JLabel specialCountLabel = new JLabel("How many special bombs each?");
    //Dark Green color
    Border blackline = BorderFactory.createLineBorder(Color.black);
    private Color darkGreen = new Color(0, 100, 0);
    private AudioPlayer audioPlayer;
    private Container buttonContainer = new Container();
    private Container titleLayout = new Container();
    private Clip musicClip;
    private JList timerCount;
    private JList specialCount;
    private Integer[] counter = {5, 10, 15, 20, 25, 30, 60};
    private Integer[] specialCounter = {0, 1, 2, 3, 4, 5, 10};
    private JScrollPane timerCountValue;
    private JScrollPane specialCountValue;

    public OptionsFrame() {
        super("Animemesweeper");
        musicClip = null;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(900, 750);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
//        setLayout(new GridBagLayout());
        getContentPane().setBackground(darkGreen);
        welcomeLabel.setIcon(yuisis);
        setButtonSettings();
        setHandlers();
        setImageOfButtons();
        setTextLocationOfButtons();
        setTimerCount();
        setSpecialBombNumber();
        setLocation();
        audioPlayer = new AudioPlayer();
        musicClip = audioPlayer.musicStart("audio/SonicGreenHillZoneTheme.wav", musicClip);
        setVisible(true);
    }

    private void setTimerCount() {
        timerCount = new JList(counter);
        timerCount.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        timerCount.setLayoutOrientation(JList.VERTICAL);
        timerCount.setVisibleRowCount(-1);
        timerCount.setSize(30, 30);
        timerCount.setSelectedIndex(0);
        timerCount.setFocusable(false);
        timerCount.setBackground(darkGreen);
        timerCountValue = new JScrollPane(timerCount);
        timerCountValue.setSize(50, 50);
        //add(timerCountValue, BorderLayout.CENTER);
    }

    private void setSpecialBombNumber() {
        specialCount = new JList(specialCounter);
        specialCount.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        specialCount.setLayoutOrientation(JList.VERTICAL);
        specialCount.setVisibleRowCount(-1);
        specialCount.setSize(30, 30);
        specialCount.setSelectedIndex(0);
        specialCount.setFocusable(false);
        specialCount.setBackground(darkGreen);
        specialCountValue = new JScrollPane(specialCount);
        specialCountValue.setSize(50, 50);
    }

    private void setLocation() {
        titleLayout.setLayout(new GridLayout(3, 3));
        welcomeLabel.setBorder(blackline);
        titleLayout.add(welcomeLabel);
        titleLayout.add(helpButton);
        countLabel.setBorder(blackline);
        titleLayout.add(countLabel);
        titleLayout.add(timerCountValue);
        specialCountLabel.setBorder(blackline);
        titleLayout.add(specialCountLabel);
        titleLayout.add(specialCountValue);
        add(titleLayout, BorderLayout.NORTH);
        buttonContainer.setLayout(new GridLayout(1, 3));
        add(buttonContainer, BorderLayout.SOUTH);
        buttonContainer.add(beginnerSelection);
        buttonContainer.add(intermediateSelection);
        buttonContainer.add(advancedSelection);
    }

    private void setButtonSettings() {
        beginnerSelection.setPreferredSize(new Dimension(250, 250));
        beginnerSelection.setBackground(Color.WHITE);
        beginnerSelection.setOpaque(true);
        intermediateSelection.setBackground(Color.WHITE);
        advancedSelection.setBackground(Color.WHITE);
        intermediateSelection.setOpaque(true);
        advancedSelection.setOpaque(true);
        intermediateSelection.setPreferredSize(new Dimension(250, 250));
        advancedSelection.setPreferredSize(new Dimension(250, 250));
        beginnerSelection.setFocusable(false);
        intermediateSelection.setFocusable(false);
        advancedSelection.setFocusable(false);
        helpButton.setFocusable(false);
    }

    private void setTextLocationOfButtons() {
        beginnerSelection.setHorizontalTextPosition(SwingConstants.CENTER);
        beginnerSelection.setVerticalTextPosition(SwingConstants.TOP);
        intermediateSelection.setHorizontalTextPosition(SwingConstants.CENTER);
        intermediateSelection.setVerticalTextPosition(SwingConstants.TOP);
        advancedSelection.setHorizontalTextPosition(SwingConstants.CENTER);
        advancedSelection.setVerticalTextPosition(SwingConstants.TOP);
    }

    private void setImageOfButtons() {
        try {
            Image img = ImageIO.read(getClass().getClassLoader().getResource("images/holoComeOn.png"));
            Image img2 = ImageIO.read(getClass().getClassLoader().getResource("images/pouty.png"));
            Image img3 = ImageIO.read(getClass().getClassLoader().getResource("images/naru.png"));
            beginnerSelection.setIcon(new ImageIcon(img));
            intermediateSelection.setIcon(new ImageIcon(img2));
            advancedSelection.setIcon(new ImageIcon(img3));
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private void setHandlers() {
        BeginnerHandler beginnerHandler = new BeginnerHandler();
        IntermediateHandler intermediateHandler = new IntermediateHandler();
        AdvancedHandler advancedHandler = new AdvancedHandler();
        HelpHandler helpHandler = new HelpHandler();
        beginnerSelection.addActionListener(beginnerHandler);
        intermediateSelection.addActionListener(intermediateHandler);
        advancedSelection.addActionListener(advancedHandler);
        helpButton.addActionListener(helpHandler);
    }

    private class BeginnerHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            int countTime = Integer.parseInt(timerCount.getSelectedValue().toString());
            int specialAmount = Integer.parseInt(specialCount.getSelectedValue().toString());
            System.out.println("Beginner");
            audioPlayer.musicStop(musicClip);
            setVisible(false);
            dispose();
            new MineSweeperGame(1, countTime, specialAmount);
        }
    }

    private class IntermediateHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            int countTime = Integer.parseInt(timerCount.getSelectedValue().toString());
            int specialAmount = Integer.parseInt(specialCount.getSelectedValue().toString());
            System.out.println("Intermediate");
            audioPlayer.musicStop(musicClip);
            setVisible(false);
            dispose();
            new MineSweeperGame(2, countTime, specialAmount);
        }
    }

    private class AdvancedHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            int countTime = Integer.parseInt(specialCount.getSelectedValue().toString());
            int specialAmount = Integer.parseInt(specialCount.getSelectedValue().toString());
            System.out.println("Advanced");
            audioPlayer.musicStop(musicClip);
            setVisible(false);
            dispose();
            new MineSweeperGame(3, countTime, specialAmount);
        }
    }

    private class HelpHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            new HelpFrame();
        }
    }
}
