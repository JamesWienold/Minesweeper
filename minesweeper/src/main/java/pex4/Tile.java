package pex4;

import java.awt.Color;
import javax.swing.JButton;
/**
 *
 * @author James Wienold
 */
public abstract class Tile extends JButton {
    public int adjacentBombs;
    protected boolean isFlagged;
    public int coordinateX;
    public int coordinateY;
    private Color darkGreen = new Color(0, 100, 0);
    public Tile() {
        setFocusable(false);
        setBackground(darkGreen);
        setOpaque(true);
        adjacentBombs = 0;
        isFlagged = false;
    }
    public Tile(int y, int x) {
        adjacentBombs = 0;
        isFlagged = false;        
        this.coordinateX = x;
        this.coordinateY = y;
    }

    public boolean isFlagged() {
        return isFlagged;
    }

    public void toggleFlag() {
        isFlagged = !isFlagged;
    }

    public void changeTheme() {
        
    }
    public abstract Tile reveal();
}
