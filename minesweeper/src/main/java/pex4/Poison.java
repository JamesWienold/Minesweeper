package pex4;

import java.awt.Color;
/**
 *
 * @author James Wienold
 */
public class Poison extends Bomb {
    private Color darkerGreen = new Color(0, 150, 0);
    public Poison() {
    }
    
    public Poison(int y, int x) {
        setName("poison");
        this.coordinateX = x;
        this.coordinateY = y;
    }
    
    @Override
    public Tile reveal() {
        setText(String.valueOf(adjacentBombs));
        setEnabled(false);
        setBackground(Color.RED);
        setIcon(null);
        return this;
    }
}
