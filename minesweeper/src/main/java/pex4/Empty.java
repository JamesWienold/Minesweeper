package pex4;

import java.awt.Color;
/**
 *
 * @author James Wienold 
 *         Describes a part of the game board that doesn't have a
 *         bomb on it. 
 */

public class Empty extends Tile {
    private Color darkerGreen = new Color(0, 150, 0);
    public Empty() {
    }

    public Empty(int y, int x) {
        setName("empty");
        //setText("E");
        this.coordinateX = x;
        this.coordinateY = y;
    }

    @Override
    public Tile reveal() {
        setText(String.valueOf(adjacentBombs));
        setEnabled(false);
        setBackground(darkerGreen);
        setIcon(null);
        return this;
    }
}
