package pex4;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.stream.Collectors;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author James Wienold
 */
public final class MineSweeperGame extends JFrame implements MouseListener {

    private Tile[][] board;
    private final int difficulty;
    private boolean checkBoard;
    private int vertical, horizontal;
    private int numberOfMines;
    private final JButton reset = new JButton("Reset");
    private final Container grid = new Container();
    private final ImageIcon flagIcon = new ImageIcon(getClass().getClassLoader().getResource("images/flag.jpg"));
    private final AudioPlayer audioPlayer;
    private Clip musicClip = null;
    private Clip musicClip2 = null;
    private Clip musicClip3 = null;
    //Timer
    private int count;
    private int defaultCount;
    private int specialTypeNumber;
    private final JLabel timerLabel = new JLabel("00:00");

    /**
     *
     * @param difficulty
     * @param timer
     * @param specialAmount
     */
    public MineSweeperGame(int difficulty, int timer, int specialAmount) {
        super("Animemesweeper LIVE!");
        count = timer;
        defaultCount = timer;
        specialTypeNumber = specialAmount;
        this.difficulty = difficulty;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setResizable(false);
        windowSize(difficulty);
        resetButtonCreation();
        fillBoard(vertical, horizontal);
        add(timerLabel, BorderLayout.SOUTH);
        audioPlayer = new AudioPlayer();
        musicClip = audioPlayer.musicStart("audio/BombermanMusicMainTheme.wav", musicClip);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     *
     * @param difficulty
     */
    public void windowSize(int difficulty) {
        switch (difficulty) {
            case 1:
                setSize(400, 420);
                vertical = 9;
                horizontal = 9;
                numberOfMines = 10;
                break;
            case 2:
                setSize(680, 700);
                vertical = 16;
                horizontal = 16;
                numberOfMines = 40;
                break;
            case 3:
                setSize(1250, 700);
                vertical = 16;
                horizontal = 30;
                numberOfMines = 99;
                break;
            default:
                setVisible(false);
                dispose();
                break;
        }
    }

    public void resetButtonCreation() {
        reset.setFocusable(false);
        add(reset, BorderLayout.NORTH);
        reset.addMouseListener(this);
    }

    public void fillBoard(int sizeVert, int sizeHorz) {
        vertical = sizeVert;
        horizontal = sizeHorz;
        board = new Tile[sizeVert][sizeHorz];
        grid.setLayout(new GridLayout(sizeVert, sizeHorz));
        for (int y = 0; y < sizeVert; y++) {
            for (int x = 0; x < sizeHorz; x++) {
                board[y][x] = new Empty(y, x);
            }
        }
        add(grid, BorderLayout.CENTER);
        board = createRandomTileTypes(board, numberOfMines);
        neighborCounter();
        for (int y = 0; y < sizeVert; y++) {
            for (int x = 0; x < sizeHorz; x++) {
                board[y][x].addMouseListener(this);
                grid.add(board[y][x]);
            }
        }
    }

    public void neighborCounter() {
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board[0].length; x++) {
                Tile tile = board[y][x];
                List<Tile> adjacentTiles = getAdjacentTiles(tile);
                tile.adjacentBombs = (int) adjacentTiles.stream().filter(t -> "bomb".equals(t.getName())).count();
            }
        }
    }

    public Tile[][] createRandomTileTypes(Tile[][] board, int mineNumber) {
        System.out.println("Mine number count: " + mineNumber);
        ArrayList<Integer> list = new ArrayList<>();
        for (int y = 0; y < vertical; y++) {
            for (int x = 0; x < horizontal; x++) {
                list.add(y * 100 + x);
            }
        }
        for (int a = 0; a < mineNumber; a++) {
            int choice = (int) (Math.random() * list.size());
            board[list.get(choice) / 100][list.get(choice) % 100] = new Bomb(list.get(choice) / 100, list.get(choice) % 100);
            list.remove(choice);
        }
        if (specialTypeNumber != 0) {
            for (int a = 0; a < specialTypeNumber; a++) {
                int choice = (int) (Math.random() * list.size());
                board[list.get(choice) / 100][list.get(choice) % 100] = new Poison(list.get(choice) / 100, list.get(choice) % 100);
                list.remove(choice);
                choice = (int) (Math.random() * list.size());
                board[list.get(choice) / 100][list.get(choice) % 100] = new Cure(list.get(choice) / 100, list.get(choice) % 100);
                list.remove(choice);
            }
        }
        return board;
    }

    public void resetBoard() {
        dispose();
        if (musicClip != null) {
            musicClip.stop();
        }
        if (musicClip2 != null) {
            musicClip2.stop();
        }
        if (musicClip3 != null) {
            musicClip3.stop();
        }
        count = defaultCount;
        new MineSweeperGame(difficulty, count, specialTypeNumber);
    }

    public void clearEmpties(Tile startingTile) {
        Queue<Tile> tilesToClear = new LinkedList<>();
        tilesToClear.add(startingTile);
        while (!tilesToClear.isEmpty()) {
            Tile tileToCheck = tilesToClear.poll();
            if (tileToCheck instanceof Empty) {
                tileToCheck.reveal();
            }
            if (tileToCheck.adjacentBombs == 0) {
                tilesToClear.addAll(getAdjacentTiles(tileToCheck));
            }
        }
    }

    public java.util.List<Tile> getAdjacentTiles(Tile ofTile) {
        ArrayList<Tile> adjacentTiles = new ArrayList<>();
        //up and left
        if (ofTile.coordinateY > 0 && ofTile.coordinateX > 0) {
            adjacentTiles.add(board[ofTile.coordinateY - 1][ofTile.coordinateX - 1]);
        }
        //left
        if (ofTile.coordinateX > 0) {
            adjacentTiles.add(board[ofTile.coordinateY][ofTile.coordinateX - 1]);
        }
        //down and left
        if (ofTile.coordinateY < board.length - 1 && ofTile.coordinateX > 0) {
            adjacentTiles.add(board[ofTile.coordinateY + 1][ofTile.coordinateX - 1]);
        }
        //up
        if (ofTile.coordinateY > 0) {
            adjacentTiles.add(board[ofTile.coordinateY - 1][ofTile.coordinateX]);
        }
        //down
        if (ofTile.coordinateY < board.length - 1) {
            adjacentTiles.add(board[ofTile.coordinateY + 1][ofTile.coordinateX]);
        }
        //up and right
        if (ofTile.coordinateY > 0 && ofTile.coordinateX < board[0].length - 1) {
            adjacentTiles.add(board[ofTile.coordinateY - 1][ofTile.coordinateX + 1]);
        }
        //right
        if (ofTile.coordinateX < board[0].length - 1) {
            adjacentTiles.add(board[ofTile.coordinateY][ofTile.coordinateX + 1]);
        }
        // down and right
        if (ofTile.coordinateY < board.length - 1 && ofTile.coordinateX < board[0].length - 1) {
            adjacentTiles.add(board[ofTile.coordinateY + 1][ofTile.coordinateX + 1]);
        }
        return adjacentTiles.stream().filter(tile -> tile.isEnabled()).collect(Collectors.toList());
    }

    public void setFlag(MouseEvent event) {
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board[0].length; x++) {
                if (board[y][x].isEnabled() == false) {
                } else {
                    if (event.getSource().equals(board[y][x])) {
                        if (board[y][x].getIcon() == flagIcon) {
                            board[y][x].setIcon(null);
                        } else {
                            board[y][x].setIcon(flagIcon);
                        }
                    }
                }
            }
        }
    }

    public void checkGameProgress(MouseEvent event) {
        System.out.println(event.getSource());
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board[0].length; x++) {
                if (event.getSource().equals(board[y][x])) {
                    if (board[y][x].getIcon() == flagIcon) {
                        board[y][x].setIcon(null);
                    }
                    if ("bomb".equals(board[y][x].getName())) {
                        lostGame();
                    } else if ("empty".equals(board[y][x].getName()) && board[y][x].isEnabled()) {
                        board[y][x].reveal();
                        clearEmpties(board[y][x]);
                        musicClip2 = audioPlayer.musicStart("audio/sugoi.wav", musicClip2);
                        checkWin();
                    } else if ("poison".equals(board[y][x].getName()) && board[y][x].isEnabled()) {
                        myTimer.start();
                        musicClip3 = audioPlayer.musicStart("audio/ohNo.wav", musicClip3);
                        board[y][x].reveal();
                    } else if ("cure".equals(board[y][x].getName()) && board[y][x].isEnabled()) {
                        myTimer.stop();
                        clearEmpties(board[y][x]);
                        musicClip3 = audioPlayer.musicStart("audio/banzai.wav", musicClip3);
                        board[y][x].reveal();
                        checkWin();
                    } else {
                        board[y][x].reveal();
                        checkWin();
                    }
                }
            }
        }
    }

    public void lostGame() {
        audioPlayer.musicStop(musicClip);
        musicClip = audioPlayer.musicStart("audio/KonoSubaMeguminExplosion.wav", musicClip);
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board[0].length; x++) {
                if (board[y][x].isEnabled()) {
                    board[y][x].reveal();
                }
            }
        }
        myTimer.stop();
        new LosingScreen();
    }

    public void checkWin() {
        boolean win = true;
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board[0].length; x++) {
                if (!"bomb".equals(board[y][x].getName()) && board[y][x].isEnabled() == true) {
                    win = false;
                    if ("cure".equals(board[y][x].getName()) && board[y][x].isEnabled() == true) {
                        win = true;
                    }
                }
            }
        }
        if (win == true) {
            audioPlayer.musicStop(musicClip);
            audioPlayer.musicStop(musicClip2);
            musicClip2 = audioPlayer.musicStart("audio/sugoiOokii.wav", musicClip2);
            for (int y = 0; y < board.length; y++) {
                for (int x = 0; x < board[0].length; x++) {
                    if (board[y][x].isEnabled()) {
                        board[y][x].reveal();
                    }
                }
            }
            new WinningScreen();
        }
    }

    public boolean checkBoard() {
        int buttonCount = 0;
        System.out.println(board.length);
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board[0].length; x++) {
                if (!board[y][x].isEnabled()) {
                    buttonCount++;
                }
            }
        }
        if (buttonCount == (vertical * horizontal)) {
            JOptionPane.showMessageDialog(null, "You already lost, stop trying.");
            return false;
        }
        if (buttonCount == (vertical * horizontal - numberOfMines)) {
            JOptionPane.showMessageDialog(null, "Yes, yes you won. Stop clicking the tiles now.");
            return false;
        }
        return true;
    }

    @Override
    public void mouseClicked(MouseEvent event) {
        System.out.println(board[0][0]);
        if (event.getButton() == MouseEvent.BUTTON1) {
            if (event.getSource().equals(reset)) {
                resetBoard();
            } else {
                checkBoard = checkBoard();
                if (checkBoard == true) {
                    checkGameProgress(event);
                }
            }
        } else if (event.getButton() == MouseEvent.BUTTON3) {
            if (event.getSource().equals(reset)) {
            } else {
                setFlag(event);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
    Timer myTimer = new Timer(1000, (ActionEvent ae) -> {
        count--;
        timerLabel.setText(count + "");
        if (count == 0) {
            lostGame();
        }
    });
}
