package pex4;

/**
 *
 * @author James Wienold
 */
public class Rainbow extends Bomb {

    public Rainbow() {
    }
    
    public Rainbow(int y, int x) {
        setText("h");
        setName("rainbow");
        this.coordinateX = x;
        this.coordinateY = y;
    }
}
