package pex4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 *
 * @author James Wienold
 */
public class WinningScreen extends JFrame {
    private JLabel thumbsUp = new JLabel();
    private JLabel winner = new JLabel("        Nice one!");
    public WinningScreen() {
        super("Winner! すごい");
        try {
            Image loserImage = ImageIO.read(getClass().getClassLoader().getResource("images/winner.png"));
            thumbsUp.setIcon(new ImageIcon(loserImage));
        } catch (IOException ex) {
            System.out.println(ex);
        }
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(900, 750);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
        setForeground(Color.BLACK);
        setResizable(false);
        winner.setFont(new Font("Courier New", Font.PLAIN, 48));        
        winner.setForeground(Color.BLACK);
        add(winner, BorderLayout.CENTER);
        add(thumbsUp, BorderLayout.SOUTH);
        setVisible(true);
    }
}