package pex4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 *
 * @author James Wienold
 */
public class LosingScreen extends JFrame {
    private JLabel explosion = new JLabel();
    private JLabel loser = new JLabel("        You lost! Wow!");
    public LosingScreen() {
        super("Explosion!");
        try {
            Image loserImage = ImageIO.read(getClass().getClassLoader().getResource("images/loser.jpg"));
            explosion.setIcon(new ImageIcon(loserImage));
        } catch (IOException ex) {
            System.out.println(ex);
        }
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(900, 750);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
        setForeground(Color.BLACK);
        setResizable(false);
        loser.setFont(new Font("Courier New", Font.PLAIN, 48));        
        loser.setForeground(Color.BLACK);
        add(loser, BorderLayout.CENTER);
        add(explosion, BorderLayout.SOUTH);
        setVisible(true);
    }
    
}
