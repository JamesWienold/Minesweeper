package pex4;

import java.awt.Color;

/**
 *
 * @author James Wienold
 */
public class Cure extends Bomb {
    private Color darkerGreen = new Color(0, 150, 0);
    public Cure() {
    }
    
    public Cure(int y, int x) {
        setName("cure");
        this.coordinateX = x;
        this.coordinateY = y;
    }
    
    @Override
    public Tile reveal() {
        setText(String.valueOf(adjacentBombs));
        setEnabled(false);
        setBackground(Color.WHITE);
        setIcon(null);
        return this;
    }
}
