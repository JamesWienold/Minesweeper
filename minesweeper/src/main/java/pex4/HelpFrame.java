package pex4;

import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JTextArea;
/**
 *
 * @author James Wienold
 */
public class HelpFrame extends JFrame {
    private final JTextArea help = new JTextArea("Beginner is 10 bombs on a 9x9 board. \nIntermediate is 40 bombs on a 16x16 board. \nAdvanced is 99 bombs on a 16x30 board. "
                                                 + "\nPoison bomb starts a timer, \nyou lose if you run out of time. \nCure bomb stops the timer.");
    private final JTextArea gameDescription = new JTextArea("Meme-eh, Minesweeper is a classic. The player assumes the role of a guy with a mine \n"
                                                      + "detector tasked to find the land mines in the field (scary job). The \"field\" is a \n"
                                                      + "two-dimensional board of squares containing either mines, or the number of mines next to \n"
                                                      + "the square. The objective of the game is to \"clear\" every square that doesn't contain \n"
                                                      + "a mine (because I don't think you want to activate a mine if this was your job). Initially, \n"
                                                      + "none of the board is cleared and the contents of every square are not visible to the \n"
                                                      + "player (spooky, huh?). To clear a square, the player clicks on it. If it's a mine, the \n"
                                                      + "player has lost (since touching mines is generally not productive.) If the square has a \n"
                                                      + "number of mines adjacent to it greater than 0, the number is revealed. If the square has \n"
                                                      + "zero mines next to it, the game clears all the nearby squares, since all squares next to a \n"
                                                      + "'0' would be safe. Here's where things get tricky: if one of the squares next to the '0' is \n"
                                                      + "also a '0', (has no mines next to it) then everything around it gets cleared as well, and \n"
                                                      + "the same goes for any newly found '0's, and so on until the \"blank\" area is 100% cleared. \n"
                                                      + "                                   Simple enough, right? Good luck!");
    public HelpFrame() {
        super("Minesweeper Rules");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        help.setFont(new Font("Helvetica", Font.PLAIN, 24));
        gameDescription.setFont(new Font("Helvetica", Font.PLAIN, 12));
        help.setEditable(false);
        gameDescription.setEditable(false);
        setSize(520, 480);
        setLocationRelativeTo(null);
        setResizable(false);
        setLayout(new BorderLayout(1, 2));
        add(help, BorderLayout.NORTH);
        add(gameDescription, BorderLayout.CENTER);
        setVisible(true);
    }
    
}
