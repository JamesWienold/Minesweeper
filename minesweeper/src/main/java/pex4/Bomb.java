package pex4;

import java.awt.Color;
import javax.swing.ImageIcon;
/**
 *
 * @author James Wienold 
 *         This is a game terminator, and much like the
 *         traditional game bomb. The player immediately 
 *         loses all life points and loses.
 */
public class Bomb extends Tile {
    private Color darkerGreen = new Color(0, 150, 0);
    private final ImageIcon bombIcon = new ImageIcon(getClass().getClassLoader().getResource("images/nozuhe.png"));
    
    public Bomb() {
        setName("bomb");
    }

    public Bomb(int y, int x) {
        setName("bomb");
        this.coordinateX = x;
        this.coordinateY = y;
    }

    @Override
    public Tile reveal() {
        setIcon(bombIcon);
        setDisabledIcon(bombIcon);
        setBackground(darkerGreen);
        setEnabled(false);
        return this;
    }
}
