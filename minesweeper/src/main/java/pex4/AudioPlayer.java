package pex4;

import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
/**
 *
 * @author James Wienold
 */
public class AudioPlayer {
    private AudioInputStream ais = null;
    public Clip musicStart(String fileName, Clip musicClip) {
        try {
                if(musicClip != null) {
                    musicClip.stop();
                }
                musicClip = AudioSystem.getClip();
                ais = AudioSystem.getAudioInputStream(getClass().getClassLoader().getResource(fileName));
                musicClip.open(ais);
            } catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
            }
            musicClip.start();
            if ("audio/BombermanMusicMainTheme.wav".equals(fileName) || "audio/SonicGreenHillZoneTheme.wav".equals(fileName)) {
                musicClip.loop(100);
        }
        return musicClip;
    }
     public void musicStop(Clip musicClip) {
         if (musicClip != null) {
         musicClip.stop();
         }
     }
}
